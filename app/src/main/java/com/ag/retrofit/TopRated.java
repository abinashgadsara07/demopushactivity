package com.ag.retrofit;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ag.retrofit.model.Adapter;
import com.ag.retrofit.model.Retrofit;
import com.ag.retrofit.network.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TopRated#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TopRated extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Retrofit m1;
    RecyclerView recyclerView2;

    public TopRated() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TopRated.
     */
    // TODO: Rename and change types and number of parameters
    public static TopRated newInstance(String param1, String param2) {
        TopRated fragment = new TopRated();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getTopRated();
        View view = inflater.inflate(R.layout.fragment_top_rated, container, false);
        recyclerView2 = view.findViewById(R.id.rv2);
        return view;
    }

    private void getTopRated() {
        ApiClient.getMovie().getToprated(getResources().getString(R.string.api_key)).enqueue(new Callback<Retrofit>() {
            @Override
            public void onResponse(Call<Retrofit> call, Response<Retrofit> response) {
                m1 = response.body();
                setRecyclerView1();
            }

            @Override
            public void onFailure(Call<Retrofit> call, Throwable t) {

            }

        });


    }
    private void setRecyclerView1 () {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView2.setLayoutManager(linearLayoutManager);
        Adapter adapter = new Adapter(getContext(), m1.getResults());
        recyclerView2.setAdapter(adapter);
    }
}