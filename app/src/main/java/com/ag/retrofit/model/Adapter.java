package com.ag.retrofit.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ag.retrofit.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    Context context;
    List<Result> Data;

    public Adapter(Context context,List<Result> play){
        this.context = context;
        this.Data = play;
    }

    public Adapter(FragmentManager supportFragmentManager, int tabCount) {
    }

    @NonNull

    @Override
    public Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_data,null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.name.setText("Movie name: \t" + Data.get(position).getTitle());
        Glide.with(context).load("https://image.tmdb.org/t/p/original" +Data.get(position).getPosterPath()).into(holder.imageView);

//        holder.overview.setText("OverView: \t"+ Data.get(position).getOverview());

    }

    @Override
    public int getItemCount() {
        return Data.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder {

        TextView name,overview;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv);
            imageView = itemView.findViewById(R.id.imageView);

        }
    }
}
