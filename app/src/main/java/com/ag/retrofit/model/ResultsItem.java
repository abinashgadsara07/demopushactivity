package com.ag.retrofit.model;

import java.util.List;

public class ResultsItem{
	private String overview;
	private String originalLanguage;
	private String originalTitle;
	private Boolean video;
	private String title;
	private List<Integer> genreIds;
	private String posterPath;
	private String backdropPath;
	private String releaseDate;
	private Double popularity;
	private Double voteAverage;
	private Integer id;
	private Boolean adult;
	private Integer voteCount;

	public void setOverview(String overview){
		this.overview = overview;
	}

	public String getOverview(){
		return overview;
	}

	public void setOriginalLanguage(String originalLanguage){
		this.originalLanguage = originalLanguage;
	}

	public String getOriginalLanguage(){
		return originalLanguage;
	}

	public void setOriginalTitle(String originalTitle){
		this.originalTitle = originalTitle;
	}

	public String getOriginalTitle(){
		return originalTitle;
	}

	public void setVideo(Boolean video){
		this.video = video;
	}

	public Boolean isVideo(){
		return video;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setGenreIds(List<Integer> genreIds){
		this.genreIds = genreIds;
	}

	public List<Integer> getGenreIds(){
		return genreIds;
	}

	public void setPosterPath(String posterPath){
		this.posterPath = posterPath;
	}

	public String getPosterPath(){
		return posterPath;
	}

	public void setBackdropPath(String backdropPath){
		this.backdropPath = backdropPath;
	}

	public String getBackdropPath(){
		return backdropPath;
	}

	public void setReleaseDate(String releaseDate){
		this.releaseDate = releaseDate;
	}

	public String getReleaseDate(){
		return releaseDate;
	}

	public void setPopularity(Double popularity){
		this.popularity = popularity;
	}

	public Double getPopularity(){
		return popularity;
	}

	public void setVoteAverage(Double voteAverage){
		this.voteAverage = voteAverage;
	}

	public Double getVoteAverage(){
		return voteAverage;
	}

	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return id;
	}

	public void setAdult(Boolean adult){
		this.adult = adult;
	}

	public Boolean isAdult(){
		return adult;
	}

	public void setVoteCount(Integer voteCount){
		this.voteCount = voteCount;
	}

	public Integer getVoteCount(){
		return voteCount;
	}
}