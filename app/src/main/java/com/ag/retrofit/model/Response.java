package com.ag.retrofit.model;

import java.util.List;

public class Response{
	private Integer page;
	private Integer totalPages;
	private List<ResultsItem> results;
	private Integer totalResults;

	public void setPage(Integer page){
		this.page = page;
	}

	public Integer getPage(){
		return page;
	}

	public void setTotalPages(Integer totalPages){
		this.totalPages = totalPages;
	}

	public Integer getTotalPages(){
		return totalPages;
	}

	public void setResults(List<ResultsItem> results){
		this.results = results;
	}

	public List<ResultsItem> getResults(){
		return results;
	}

	public void setTotalResults(Integer totalResults){
		this.totalResults = totalResults;
	}

	public Integer getTotalResults(){
		return totalResults;
	}
}