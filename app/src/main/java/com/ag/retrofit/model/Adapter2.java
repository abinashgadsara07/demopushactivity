package com.ag.retrofit.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adapter2 extends FragmentPagerAdapter {

    private final ArrayList<Fragment> fragList = new ArrayList<>();
    private final ArrayList<String> frag = new ArrayList<>();

    public Adapter2(FragmentManager f,int b){
        super(f,b);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragList.get(position);
    }

    @Override
    public int getCount() {
        return fragList.size();
    }

    public void add(Fragment fragment,String ttl){
            fragList.add(fragment);
            frag.add(ttl);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return frag.get(position);
    }
}
