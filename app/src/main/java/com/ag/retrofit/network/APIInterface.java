package com.ag.retrofit.network;

import com.ag.retrofit.model.Retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {
    @GET("now_playing?")
    Call<Retrofit> getMovies(@Query("api_key")String key);

    @GET("top_rated?")
    Call<Retrofit> getToprated(@Query("api_key")String key);
}
