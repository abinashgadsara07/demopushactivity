package com.ag.retrofit.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static APIInterface getMovie(){

        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create()).build();
         APIInterface api = retrofit.create(APIInterface.class);
         return api;
    }
}